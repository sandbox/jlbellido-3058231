<?php

namespace Drupal\webform_email_confirmer;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\webform\WebformSubmissionStorage;
use Drupal\webform\WebformInterface;

/**
 * The WebformEmailConfirmerSubmissionStorage class.
 */
class WebformEmailConfirmerSubmissionStorage extends WebformSubmissionStorage {

  /**
   * {@inheritdoc}
   */
  public function getColumns(WebformInterface $webform = NULL, EntityInterface $source_entity = NULL, AccountInterface $account = NULL, $include_elements = TRUE) {
    $columns = parent::getColumns($webform, $source_entity, $account, $include_elements);

    $columns['email_confirmation_status'] = [
      'title' => $this->t('Email Confirmation Status'),
      'name' => 'email_confirmation_status',
      'format' => 'value',
    ];

    return $columns;
  }

}
