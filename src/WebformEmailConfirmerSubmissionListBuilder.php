<?php

namespace Drupal\webform_email_confirmer;

use Drupal\webform\WebformSubmissionListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * The WebformEmailConfirmerSubmissionListBuilder class.
 */
class WebformEmailConfirmerSubmissionListBuilder extends WebformSubmissionListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildRowColumn(array $column, EntityInterface $entity) {
    $name = $column['name'];
    switch ($name) {
      case 'email_confirmation_status':
        $row = '';
        if (!$entity->email_confirmation_status->isEmpty()) {
          $allowed_values = $entity
            ->getFieldDefinition('email_confirmation_status')
            ->getSettings()['allowed_values'];
          $row = $allowed_values[$entity->email_confirmation_status->value];
        }
        break;

      default:
        $row = parent::buildRowColumn($column, $entity);
        break;
    }

    return $row;
  }

}
