<?php

namespace Drupal\webform_email_confirmer\Plugin\WebformHandler;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\email_confirmer\EmailConfirmerManager;
use Drupal\webform\Plugin\WebformElementManagerInterface;
use Drupal\webform\Plugin\WebformHandler\EmailWebformHandler;
use Drupal\webform\WebformSubmissionConditionsValidatorInterface;
use Drupal\webform\WebformSubmissionInterface;
use Drupal\webform\WebformThemeManagerInterface;
use Drupal\webform\WebformTokenManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Send a confirm email when a webform submission.
 *
 * @WebformHandler(
 *   id = "email_confirmer",
 *   label = @Translation("Email Confirmer"),
 *   category = @Translation("Notification"),
 *   description = @Translation("Sends an email confirmation when a new submission is created."),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_SINGLE,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 *   submission = \Drupal\webform\Plugin\WebformHandlerInterface::SUBMISSION_OPTIONAL,
 *   tokens = TRUE,
 * )
 */
class EmailConfirmerWebformHandler extends EmailWebformHandler {

  /**
   * The EmailConfirmer.
   *
   * @var \Drupal\email_confirmer\EmailConfirmerManager
   */
  protected $emailConfirmer;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, LoggerChannelFactoryInterface $logger_factory, ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager, WebformSubmissionConditionsValidatorInterface $conditions_validator, AccountInterface $current_user, ModuleHandlerInterface $module_handler, LanguageManagerInterface $language_manager, MailManagerInterface $mail_manager, WebformThemeManagerInterface $theme_manager, WebformTokenManagerInterface $token_manager, WebformElementManagerInterface $element_manager, EmailConfirmerManager $email_confirmer) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $logger_factory, $config_factory, $entity_type_manager, $conditions_validator, $current_user, $module_handler, $language_manager, $mail_manager, $theme_manager, $token_manager, $element_manager);
    $this->emailConfirmer = $email_confirmer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('logger.factory'),
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('webform_submission.conditions_validator'),
      $container->get('current_user'),
      $container->get('module_handler'),
      $container->get('language_manager'),
      $container->get('plugin.manager.mail'),
      $container->get('webform.theme_manager'),
      $container->get('webform.token_manager'),
      $container->get('plugin.manager.webform.element'),
      $container->get('email_confirmer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $configuration = parent::defaultConfiguration();

    return $configuration + [
      'clear_canceled' => TRUE,
      'clear_pending' => 86400,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    // Hide CC & BCC fields because this mail can be send only to 1 address.
    $form['to']['cc_mail']['#access'] = FALSE;
    $form['to']['bcc_mail']['#access'] = FALSE;

    $form['clear'] = [
      '#type' => 'details',
      '#title' => $this->t('Clear submissions'),
      '#open' => TRUE,
    ];

    $form['clear']['clear_canceled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Clear submissions on cancel'),
      '#parents' => ['settings', 'clear_canceled'],
      '#default_value' => $this->configuration['clear_canceled'],
    ];

    $options = [3600, 10800, 21600, 43200, 86400, 604800];

    $form['clear']['clear_pending'] = [
      '#type' => 'webform_select_other',
      '#title' => $this->t('Clear pending submissions after a period of time'),
      '#options' => [0 => t('Never')] + array_map([\Drupal::service('date.formatter'), 'formatInterval'], array_combine($options, $options)),
      '#other__title' => 'Enter other',
      '#other__title_display' => 'hidden',
      '#other__placeholder' => $this->t('Enter interval in seconds…'),
      '#other__type' => 'textfield',
      '#other__size' => 60,
      '#other__allow_tokens' => FALSE,
      '#required' => TRUE,
      '#parents' => ['settings', 'clear_pending'],
      '#default_value' => $this->configuration['clear_pending'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(WebformSubmissionInterface $webform_submission, $update = TRUE) {
    $state = $webform_submission->getWebform()->getSetting('results_disabled') ? WebformSubmissionInterface::STATE_COMPLETED : $webform_submission->getState();
    if ($this->configuration['states'] && in_array($state, $this->configuration['states'])) {
      $email = $this->getConfirmationMail($webform_submission);
      if (!empty($email)) {
        $realm = $this->getConfirmationRealm($webform_submission);
        $confirmation = $this->emailConfirmer->getConfirmation($email, 'pending', $realm);
        if (empty($confirmation)) {
          // We generate a new confirmation notification:
          $confirmation = $this->emailConfirmer->createConfirmation($email)
            ->setProperty('webform_submission_id', $webform_submission->id())
            ->setProperty('webform_handler_id', $this->handler_id)
            ->setRealm($realm);
          $confirmation->save();
        }
      }
      $message = $this->getMessage($webform_submission);
      $this->sendMessage($webform_submission, $message);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getMessage(WebformSubmissionInterface $webform_submission) {
    $message = parent::getMessage($webform_submission);

    // Ensure that email is sent only to the right email only.
    $email = $this->getConfirmationMail($webform_submission);
    $message['to_mail'] = $email;
    // Replace any possible additional email_confirmer_confirmation tokens.
    $realm = $this->getConfirmationRealm($webform_submission);
    $confirmation = $this->emailConfirmer->getConfirmation($email, 'pending', $realm);
    $context = ['email_confirmer_confirmation' => $confirmation];

    $message['body'] = $this->tokenManager->replace(
      $message['body'],
      $webform_submission,
      $context,
      ['sanitize' => FALSE]
    );

    // Change the recipient to the the email user:
    $message['to_mail'] = $email;

    return $message;
  }

  /**
   * {@inheritdoc}
   */
  protected function buildTokenTreeElement(array $token_types = [], $description = NULL) {
    $token_types[] = 'email-confirmer';
    return parent::buildTokenTreeElement($token_types, $description);
  }

  /**
   * {@inheritdoc}
   */
  protected function elementTokenValidate(array &$form, array $token_types = ['webform', 'webform_submission', 'webform_handler']) {
    $token_types[] = 'email-confirmer';
    return parent::elementTokenValidate($form, $token_types);
  }

  /**
   * Retrieves a realm string based on the WebformSubmission.
   *
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   *   The WebformSubmissionInterface object.
   *
   * @return string
   *   The unique realm for the given WebformSubmissionInterface.
   */
  protected function getConfirmationRealm(WebformSubmissionInterface $webform_submission) {
    return 'webform_email_confirmer_' . $webform_submission->id();
  }

  /**
   * Retrieves the confirmation mail based on the WebformSubmission.
   *
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   *   The WebformSubmissionInterface object.
   *
   * @return string
   *   The unique realm for the given WebformSubmissionInterface.
   */
  protected function getConfirmationMail(WebformSubmissionInterface $webform_submission) {
    $emails = $this->getMessageEmails($webform_submission, 'to', $this->configuration['to_mail']);
    return $emails[0];
  }

}
